from modeltranslation.translator import register, TranslationOptions
from .models import Post, Comments


@register(Post)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('title', 'content')