from django import template
from django.utils.html import mark_safe


register = template.Library()

def get_children(qs_chid):
    res = []
    for comment in qs_chid:
        c = {
            'id': comment.id,
            'content': comment.content,
            'created_on': comment.created_on.strftime('%Y-%m-%d %H:%m'),
            'author': comment.author,
            'is_child': comment.is_child,
            'parent_id': comment.get_parent
        }
        if comment.comment_children.exists():
            c['children'] = get_children(comment.comment_children.all())
        res.append(c)
    return res
    
@register.filter()
def create_coments_tree(qs):
    res = []
    for comment in qs:
        c = {
            'id': comment.id,
            'content': comment.content,
            'created_on': comment.created_on.strftime('%Y-%m-%d %H:%m'),
            'author': comment.author,
            'is_child': comment.is_child,
            'parent_id': comment.get_parent
        }
        if comment.comment_children:
            c['children'] = get_children(comment.comment_children.all())
        if not comment.is_child:
            res.append(c)
    return res

@register.filter()
def comments_filter(comments_list):
    res = """
        <ul>
        {}
        </ul>
        """
    i = ''
    for comment in comments_list:
        i += """
            <li>
            {comment_id}
            </li>
            """.format(comment_id=comment['id'])
        if comment.get('children'):
            i += comments_filter(comment['children'])
    return mark_safe(res.format(i))



"""
from django import template

register = template.Library()

@register.filter()
def tree(obj):
    print(obj)
"""