from .models import Post, Comments
from django.forms import ModelForm, TextInput, Textarea


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = ["title", "slug", "content", "status", "news", "image_articles"]

        widgets = {
            "title": TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Введите название'}),

            "slug": TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Введите url статьи'}),
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comments
        fields = ('content',)

        widgets = {
            "content": Textarea(attrs={
            'class': 'form-control text-light',
            'placeholder': 'Вводи',
            'style': 'background-color: #302e3dc9; border-color: rgba(126, 136, 136, 0.651);',
            })
        }