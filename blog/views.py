from django.views import generic
from .models import Post, Comments, Dislike, Like
from .forms import PostForm, CommentForm
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin #for Login access
from django.views.generic.edit import FormMixin
from django.http import HttpResponseRedirect
from .likes import add_like, remove_like, is_fan, get_fans
"""
class PostListArticles(generic.ListView):
    #queryset = Post.objects.filter(status=1).order_by('-created_on')
    template_name = 'blog/articles.html'
    #context_object_name = 'list_articles'

    def get_queryset(self):
        queryset = Post.objects.all()
        queryset0 = queryset.filter(news=0,status=1).order_by('-created_on')
        return queryset0
"""

from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger


class PostListArticles(generic.ListView):
    model = Post
    template_name = "blog/articles.html"
    paginate_by = 20
    
    def get_context_data(self, **kwargs):
        context = super(PostListArticles, self).get_context_data(**kwargs) 
        list_exam = Post.objects.all()
        paginator = Paginator(list_exam, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            file_exams = paginator.page(page)
        except PageNotAnInteger:
            file_exams = paginator.page(1)
        except EmptyPage:
            file_exams = paginator.page(paginator.num_pages)
            
        context['list_exams'] = file_exams
        return context

    def get_queryset(self):
        queryset = Post.objects.all()
        queryset0 = queryset.filter(news=0,status=1).order_by('-created_on')
        return queryset0


class PostListProducts(generic.ListView):
    #queryset = Post.objects.filter(status=1).order_by('-created_on')
    template_name = 'blog/products.html'
    model = Post
    paginate_by = 20
    
    def get_context_data(self, **kwargs):
        context = super(PostListProducts, self).get_context_data(**kwargs) 
        list_exam = Post.objects.all()
        paginator = Paginator(list_exam, self.paginate_by)

        page = self.request.GET.get('page')

        try:
            file_exams = paginator.page(page)
        except PageNotAnInteger:
            file_exams = paginator.page(1)
        except EmptyPage:
            file_exams = paginator.page(paginator.num_pages)
            
        context['list_exams'] = file_exams
        return context

    def get_queryset(self):
        queryset = Post.objects.all()
        queryset0 = queryset.filter(news=1,status=1).order_by('-created_on')
        return queryset0


class PostDetail(FormMixin, generic.DetailView):
    model = Post
    template_name = 'blog/post_detail.html'
    form_class = CommentForm
    #success_url = reverse_lazy('post_detail')

    def get_success_url(self,**kwargs):
        return reverse_lazy('post_detail',kwargs={'slug': self.get_object().slug})

    def post(self,request,*args,**kwargs):
        form = self.get_form()
        if form.is_valid:
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self,form):
        self.object = form.save(commit=False)
        self.object.post = self.get_object()
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)

    def get_context_data(self,**kwargs):
        kwargs['list_articles'] = Post.objects.all().order_by('-id')
        kwargs["liked"] = 'lol'
        return super().get_context_data(**kwargs)

class LikeViewClass():
    @classmethod
    def LikeView(cls,request,slug):
        namePOST = ['comment_id_like','post_id_dislike','comment_id_dislike','post_id_like']
        nameMODEL = {'post': Post, 'comment': Comments}
        nameMODEL0 = {'like': Like, 'dislike': Dislike}
        for i in namePOST:
            if request.POST.get(i):
                model = i.split('_')[0]
                model0 = i.split('_')[2]
                for nameI in nameMODEL:
                    if model == nameI:
                        obj0 = get_object_or_404(nameMODEL[model], id=request.POST.get(i))
                for nameII in nameMODEL0:
                    if model0 == nameII:
                        if is_fan(obj0, request.user, Like) == False and is_fan(obj0, request.user, Dislike) == False:
                            add_like(obj0, request.user, nameMODEL0[model0])

                        elif is_fan(obj0, request.user, Like) == True and is_fan(obj0, request.user, Dislike) == False:
                            if model0 == 'dislike':
                                remove_like(obj0, request.user, Like)
                                add_like(obj0, request.user, Dislike)
                            if model0 == 'like':

                                remove_like(obj0, request.user, Like)

                        elif is_fan(obj0, request.user, Like) == False and is_fan(obj0, request.user, Dislike) == True:
                            if model0 == 'like':
                                remove_like(obj0, request.user, Dislike)
                                add_like(obj0, request.user, Like)
                            if model0 == 'dislike':
                                remove_like(obj0, request.user, Dislike)


        return HttpResponseRedirect(reverse('post_detail', args=[str(slug)]))



class PostEditPageView(LoginRequiredMixin, generic.ListView):
    queryset = Post.objects.all()
    template_name = 'blog/edit_page.html'
    context_object_name = 'list_articles'

    def dispatch(self, request, *args, **kwargs):
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

class PostCreateView(LoginRequiredMixin, generic.CreateView):
    template_name = 'blog/create.html'
    model = Post
    form_class = PostForm
    success_url = reverse_lazy('blog_edit_page')

    def get_context_data(self,**kwargs):
        kwargs['list_articles'] = Post.objects.all().order_by('-id')
        return super().get_context_data(**kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        return kwargs

    # Чтобы сохранить статью под данным пользователем 
    def form_valid(self,form):
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        self.object.save()
        return super().form_valid(form)

class PostEditView(LoginRequiredMixin, generic.UpdateView):
    model = Post
    template_name = 'blog/edit.html'
    form_class = PostForm
    success_url = reverse_lazy('blog_edit_page')

    # Чтобы редактировать только свои статьи
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        if self.request.user != kwargs['instance'].author:
            return self.handle_no_permission()
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        return kwargs

class PostDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Post
    success_url = reverse_lazy('blog_edit_page')

    # Чтобы удалять только свои статьи
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user != self.object.author:
            return self.handle_no_permission()
        if not self.request.user.is_superuser and not self.request.user.is_staff:
            return self.handle_no_permission()
        self.object.delete()
        return redirect('blog_edit_page')



"""
def delete(request,pk):
    get_articles = Post.objects.get(pk=pk)
    get_articles.delete()
    return redirect('blog_edit_page')
"""


"""
def edit_page(request):
    context = {
        'list_articles': Post.objects.all().order_by('-id')
    }
    return render(request, 'blog/edit_page.html', context)
"""
"""
def edit(request, pk):
    get_articles = Post.objects.get(pk=pk)

    if request.method == 'POST':
        form = PostForm(request.POST,instance=get_articles)
        if form.is_valid():
            form.save()
            return redirect('blog_edit_page')

    context = {
        'get_articles': get_articles,
        'form': PostForm(instance=get_articles) #Подстановка данных в форму
    }
    return render(request,'blog/edit.html', context)
"""
"""
def create(request):
    error = ''
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('blog')
        else:
            error = 'Форма была говной'

    form = PostForm()
    context = {
        'form': form,
        'error': error
    }
    return render(request, 'blog/create.html', context)
"""