from django.contrib import admin
from .models import Post, Comments, Like, Dislike
from modeltranslation.admin import TranslationAdmin

class PostAdmin(TranslationAdmin):
    list_display = ('title','slug','status','author','created_on')
    list_filter = ("status","news")
    search_fields = ['title', 'content']
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Dislike)
admin.site.register(Like)
admin.site.register(Comments)
admin.site.register(Post, PostAdmin)