from django.db import models
from django.conf import settings
#from django.contrib.auth.models import User

#for ckeditor
#from ckeditor.fields import RichTextUploadingField
from ckeditor_uploader.fields import RichTextUploadingField
#for ckeditor

# For likes model
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
# For likes model

class Like(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
    related_name='likes',
    on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

class Dislike(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
    related_name='dislikes',
    on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

class Post(models.Model):
    STATUS = (
        (0,"Draft"),
        (1,"Publish")
    )

    NEWS = (
        (0,"Articles"),
        (1,"Product"),
        (2,"HOME")
    )

    title = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=200, unique=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete= models.CASCADE, blank=True, null=True)
    updated_on = models.DateTimeField(auto_now= True)
    content = RichTextUploadingField(blank=True, null=True, verbose_name='Контент', external_plugin_resources=[('youtube','/static/ckeditor/youtube/','plugin.js',)],)
    #content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    status = models.IntegerField(choices=STATUS, default=0)
    news = models.IntegerField(choices=NEWS, default=0)
    image_articles = models.ImageField(blank=True, null=True)
    #likes = models.ManyToManyField(User, related_name='blog_like')
    
    dislikes = GenericRelation(Dislike)
    likes = GenericRelation(Like)

    @property
    def total_likes(self):
        return self.likes.count()

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.title

class Comments(models.Model):
    post = models.ForeignKey(Post, on_delete= models.CASCADE, verbose_name='Пост', related_name='comments_post', blank=True, null=True)
    # Через поле 'author' можно попасть в 'User', а дальше в 'profile'
    # В ШОКЕ !!!!
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, verbose_name='Автор', blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    content = models.TextField(verbose_name='Комментарий')
    status = models.BooleanField(verbose_name='Visible', default='True')

    likes = GenericRelation(Like)
    dislikes = GenericRelation(Dislike)