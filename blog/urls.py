from . import views
from django.urls import path

urlpatterns = [
#    path('', views.PostListArticles.as_view(), name='blog'),
    path('articls/<int:page>/', views.PostListArticles.as_view(), name='blog_articls'),
    path('products/<int:page>/', views.PostListProducts.as_view(), name='blog_products'),
    path('edit_page/', views.PostEditPageView.as_view(), name='blog_edit_page'),
    path('edit/<int:pk>/', views.PostEditView.as_view(), name='blog_edit'),
    path('create/', views.PostCreateView.as_view(), name='blog_create_articles'),
    path('delete/<int:pk>/', views.PostDeleteView.as_view(), name='blog_delete'),
    path('<slug:slug>/', views.PostDetail.as_view(), name='post_detail'),
    path('like/<slug:slug>', views.LikeViewClass.LikeView, name='like_post'),
]