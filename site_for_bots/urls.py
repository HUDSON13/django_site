"""site_for_bots URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

#for ckeditor
from django.conf import settings
from django.conf.urls.static import static
#for ckeditor

urlpatterns = [
    path('admin/', admin.site.urls, name = 'admin_page'),
    path('', include('main.urls'), name = 'root'),
    path('accounts/', include('accounts.urls')),
    path('blog/', include('blog.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('social/', include('allauth.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) #for ckeditor

# для перевода
from django.conf.urls.i18n import i18n_patterns
urlpatterns += i18n_patterns(
    path('', include('main.urls')),
    path('accounts/', include('accounts.urls')),
    path('blog/', include('blog.urls')),
    path('social/', include('allauth.urls')),
)