from django.core.context_processors import request
def get_current_path(request):
  return {
      'current_path': request.get_full_path()
    }