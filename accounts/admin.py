from django.contrib import admin
from .models import Profile, Friend_Request


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'about_me', 'avatar', 'slug')
    #prepopulated_fields = {'slug': ('user',)}

admin.site.register(Profile, ProfileAdmin)

class Friend_RequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'from_user', 'to_user')
admin.site.register(Friend_Request, Friend_RequestAdmin)

