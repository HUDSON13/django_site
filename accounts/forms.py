# Подключаем компонент для работы с формой
from django import forms
# Подключаем компонент UserCreationForm
from django.contrib.auth.forms import UserCreationForm
# Подключаем модель User
from django.contrib.auth.models import User
from django.conf import settings
from .models import Profile
from django.forms import ModelForm, TextInput, Textarea, ImageField

class ProfileForm(forms.ModelForm):
  avatar = ImageField()
  class Meta:
    model = Profile
    fields = ("avatar", "about_me")

    widgets = {
        'about_me': Textarea(attrs={
          'class': 'form-control text-light',
          'placeholder': 'О себе',
          'style': 'background-color: #302e3dc9; border-color: rgba(126, 136, 136, 0.651);'})
    }

class UserForm(forms.ModelForm):
  class Meta:
    model = User
    fields = ("username", "email")
    widgets = {
      'username': TextInput(attrs={
        'class': 'form-control text-light',
        'style': 'background-color: #302e3dc9; border-color: rgba(126, 136, 136, 0.651);'})
    }

class RegistrForm(UserCreationForm):
  email = forms.EmailField(max_length=254)
  class Meta:
    model = User
    fields = ('username', 'email', 'password1', 'password2', )
