from django.contrib.auth import views as auth_views
from . import views
from django.urls import path
from django.contrib import admin

urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    path('registr/', views.regist, name='registr'),
    path('password-change/', auth_views.PasswordChangeView.as_view(), name = 'password_change'),
    path('password-change/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('password-reset/', auth_views.PasswordResetView.as_view(), name='password_reset'),
    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('edit/', views.profile_edit, name='profile_edit'),
#    path('<slug:slug>/', views.profile_view.as_view(), name='profile_view'),
    path('<slug:slug>/', views.account_view, name='profile_view'),
    #send_friend_request
    path('SFR/<int:id>/', views.send_friend_request, name='SFR'),
    #decline_friend_request
    path('DFR/<int:id>/', views.decline_friend_request, name='DFR'),
    #accept_friend_request
    path('AFR/<int:id>/', views.accept_friend_request, name='AFR'),
    #delete_friens
    path('DF/<int:id>/', views.delete_friens, name='DF'),
    path('-/chat_ls/<to_user>', views.chat, name='chat_ls')
]