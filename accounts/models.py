from django.db import models
from django.conf import settings
from django.db.models.signals import post_save
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(default='Centropolis.jpg')
    about_me = models.TextField(blank=True, null=True)
    #email = models.EmailField(blank=True, null=True)
    slug = models.CharField(max_length=100, unique=True)
    friends = models.ManyToManyField(User, related_name='friends', blank=True)

    def __str__(self):
        return self.slug
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.user)
        super(Profile, self).save(*args, **kwargs)
    
# Для создания профиля в момент создания пользователя в дефолтной моделе django
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

post_save.connect(create_user_profile, sender=User)

class Chat(models.Model):
    from_user = models.ForeignKey(User, related_name='c_from_user', on_delete=models.CASCADE)
    to_user = models.ForeignKey(User, related_name='c_to_user', on_delete=models.CASCADE)

class Friend_Request(models.Model):
    from_user = models.ForeignKey(User, related_name='from_user', on_delete=models.CASCADE)
    to_user = models.ForeignKey(User, related_name='to_user', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.from_user)