from django import template
from ..models import Profile


register = template.Library()
    
@register.filter()
def get_avatar(qs):
    return Profile.objects.get(slug=qs).avatar


"""
from django import template

register = template.Library()

@register.filter()
def tree(obj):
    print(obj)
"""