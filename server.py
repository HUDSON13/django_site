import uvicorn
import os
import django

from channels.routing import get_default_application


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "site_for_bots.settings")
django.setup()

app = get_default_application()


if __name__ == "__main__":
   uvicorn.run("site_for_bots.asgi:application", reload=True, host="0.0.0.0", port=8000, ws="auto", workers=2, log_level="info", ssl_keyfile='data/cert/ca.key', ssl_certfile='data/cert/ca.pem',)