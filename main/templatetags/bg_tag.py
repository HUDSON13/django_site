from django import template
from main.models import Background



register = template.Library()

@register.simple_tag()
def get_bg(all=False):
    if all==True:
        return Background.objects.all()
    return Background.objects.all().filter(status=1)

