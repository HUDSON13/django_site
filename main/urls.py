from django.urls import path
from . import views


urlpatterns = [
    path('about', views.about, name='about'),
    #path('create', views.create, name='create'),
    #path('articles', views.articles, name='articles'),
    #path('', views.room, name='home'),
    path('', views.index, name='home'),
    #path('chat/', views.chat, name='chat'),
    #path('chat/<str:room_name>/', views.room, name='room'),
]