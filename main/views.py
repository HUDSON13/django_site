from django.shortcuts import render, redirect
from .models import ex_data
from .forms import ex_dataForm
from django.views import generic

from blog.models import Post
from blog import views as blog_views

from .models import Message, About_us

def chat(request):
    return render(request, 'main/chat.html')

def index(request, room_name='main'):
    #username = request.user.username
    post = Post.objects.all().filter(news=2,status=1).order_by('-created_on')
    username = request.GET.get('username', 'Anonymous')
    messages = Message.objects.filter(room=room_name).reverse()[:50:-1]

    return render(request, 'main/index.html', {'room_name': room_name, 'username': username, 'messages': messages, 'post': post})

def about(request):
    about = About_us.objects.all()
    return render(request, 'main/about.html', {'about': about})

def create(request):
    error = ''
    if request.method == 'POST':
        form = ex_dataForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        else:
            error = 'Форма была говной'

    form = ex_dataForm()
    context = {
        'form': form,
        'error': error
    }
    return render(request, 'main/create.html', context)

def articles(request):
    ex_datas = ex_data.objects.order_by('-id')
    return render(request, 'main/articles.html', {'title': 'Статьи', 'ex_datas': ex_datas})