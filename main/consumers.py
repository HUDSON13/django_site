import json

from channels.generic.websocket import AsyncWebsocketConsumer
from asgiref.sync import sync_to_async

from .models import Message
from accounts.models import Profile

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'chat_%s' % self.room_name

        # Join room
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()
    
    async def disconnect(self, close_code):
        # Leave room
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
    
    # Receive message from web socket
    async def receive(self, text_data):
        data = json.loads(text_data)
        print(data)
        message = data['message']
        username = data['username']
        #avatar = data['avatar']
        slug = data['slug']
        room = data['room']

        await self.save_message(room, message)

        # Send message to room group
        avatar = await self.get_avatar()
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message,
                'username': username,
                'avatar': avatar,
                'slug': slug
            }
        )
    
    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        username = event['username']
        avatar = event['avatar']
        slug = event['slug']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'username': username,
            'avatar': avatar,
            'slug': slug

        }))

    @sync_to_async
    def get_avatar(self):
        return str(Profile.objects.get(user=self.scope['user']).avatar)

    @sync_to_async
    def save_message(self, room, message):
        Message.objects.create(username=self.scope['user'], room=room, content=message)