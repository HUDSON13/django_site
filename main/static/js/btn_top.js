// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    document.getElementById("myBtn").style.display = "block";
} else {
    document.getElementById("myBtn").style.display = "none";
}
}

// When the user clicks on the button, scroll to the top of the document

function up() {
	var top = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
  if(top > 0) {
	window.scrollBy(0,((top+100)/-10));
	t = setTimeout('up()',20);
  } else clearTimeout(t);
  return false;
}