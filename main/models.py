from django.db import models
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
#from django.contrib.auth.models import User

#for ckeditor
#from ckeditor.fields import RichTextUploadingField
from ckeditor_uploader.fields import RichTextUploadingField
#for ckeditor

# Create your models here.
class ex_data(models.Model):
    title = models.CharField('Название', max_length=50)
    #data = models.TextField(blank=True, verbose_name='Контент')
    #data = RichTextField(blank=True, verbose_name='Контент')
    data = RichTextUploadingField(blank=True, null=True, verbose_name='Контент', external_plugin_resources=[('youtube','/static/ckeditor/youtube/','plugin.js',)],)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Данные'
        verbose_name_plural = 'Данные'

class Message(models.Model):
    username = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='message_user', blank=True, null=True)
    room = models.CharField(max_length=255, default='ALL_SITE_CHAT')
    content = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.content

    class Meta:
        ordering = ('date_added',)


def user_directory_path(instance, filename):
    return "bg" + '/' + filename

class Background(models.Model):

    STATUS = (
        (0,"Draft"),
        (1,"Publish")
        )
    bg = models.ImageField(upload_to=user_directory_path)
    status = models.IntegerField(choices=STATUS, default=0)

    def __str__(self):
        return str(self.bg)

class About_us(models.Model):
    title = models.CharField('Заголовок', max_length=50)
    content = RichTextUploadingField(blank=True, null=True, verbose_name='Контент', external_plugin_resources=[('youtube','/static/ckeditor/youtube/','plugin.js',)],)

    def __str__(self):
        return self.title