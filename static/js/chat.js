function scrollToBottom() {
    let objDiv = document.getElementById("chat-messages");
    objDiv.scrollTop = objDiv.scrollHeight;
}

scrollToBottom();

const roomName = JSON.parse(document.getElementById('json-roomname').textContent);
const userName = JSON.parse(document.getElementById('json-username').textContent);


//const avatarName = JSON.parse(document.getElementById('json-avatar').textContent);
const slugName = JSON.parse(document.getElementById('json-slug').textContent);

const chatSocket = new WebSocket(
    'wss://'
    + window.location.host
    + '/ws/'
    + roomName
    + '/'
);

chatSocket.onmessage = function(e) {
    console.log('onmessage');

    const data = JSON.parse(e.data);

    if (data.message) {
        document.querySelector('#chat-messages').innerHTML += (
             '<tr>' +
                 '<td>' +
                   '<a href="' + 'http://' + window.location.host + '/accounts/' + data.slug + '"><img src="/media/' + data.avatar + '" class="rounded-circle z-depth-0" alt="avatar image" height="30" width="30"></a>&emsp;' +
                   '<a style="font-size:14px" class="text-secondary" href="' + 'http://' + window.location.host + '/accounts/' + data.slug + '">' + data.username + '|' + new Date().getHours() + ':' + new Date().getMinutes() + ':' + new Date().getSeconds() + '</a>' +
                 '</td>' +
             '</tr>' +
             '<tr>' +
                 '<td colspan="2"><p class="text-light" style="font-size:14px">&emsp;' + data.message + '</p></td>' +
             '</tr>'
        );
    } else {
        alert('The message is empty!');
    }

    scrollToBottom();
};

chatSocket.onclose = function(e) {
    console.log('The socket close unexpectadly');
};

document.querySelector('#chat-message-submit').onclick = function(e) {
    const messageInputDom = document.querySelector('#chat-message-input');
    const message = messageInputDom.value;

    chatSocket.send(JSON.stringify({
        'message': message,
        'username': userName,
        //'avatar': avatarName,
        'slug': slugName,
        'room': roomName
    }));

    messageInputDom.value = '';
};